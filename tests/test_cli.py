import os

import pytest
from cli_test_helpers import ArgvContext, EnvironContext
from click.testing import CliRunner

import ranchercli
from ranchercli import cli
from ranchercli import Logger


# def test_entrypoint():
#     """
#     Is entrypoint script installed? (setup.py)
#     """
#     exit_status = os.system('ranchertool --help')
#     assert exit_status == 0


def test_cli():
    """
    Does CLI stop execution w/o a command argument?
    """
    with ArgvContext('gitlab_rancher_util'), pytest.raises(SystemExit):
        ranchercli.cli()
        pytest.fail("CLI doesn't abort asking for a command argument")


def test_run_as_module():
    """
    Can this package be run as a Python module?
    """
    exit_status = os.system('python -m ranchercli --help')
    assert exit_status == 0


def test_fail():
    message_regex = "Error: Missing option '--rancher-url'."
    with ArgvContext('gitlab_rancher_util'), pytest.raises(SystemExit):
        ranchercli.cli()
        pytest.fail("CLI didn't abort")


def test_noargs():
    message_regex = "Usage: cli [OPTIONS] COMMAND [ARGS]..."
    runner = CliRunner()
    result = runner.invoke(ranchercli.cli)
    log = Logger("DEBUG")
    log.info("OUTPUT: \r\n\r\n"+result.output)
    assert result.exit_code == 0
    assert message_regex in result.output


def test_bogus():
    message_regex = "Error: No such command 'bogus'."
    runner = CliRunner()
    result = runner.invoke(ranchercli.cli, "bogus")
    log = Logger("DEBUG")
    log.info("OUTPUT: \r\n\r\n"+result.output)
    assert result.exit_code == 2
    assert message_regex in result.output


def test_service_status():
    message_regex = "Usage: ranchercli service status [OPTIONS]"
    runner = CliRunner()
    result = runner.invoke(ranchercli.cli, ['service', 'status', '-h'], "", None, True, True)
    log = Logger("DEBUG")
    log.info("OUTPUT: \r\n\r\n"+result.output)
    assert result.exit_code == 0
    assert message_regex in result.output
