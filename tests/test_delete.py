from cli_test_helpers import EnvironContext
from click.testing import CliRunner

import ranchercli
from ranchercli import Logger, LogLevel
from ranchercli import cli


def test_delete():
    logger = Logger(name='test_cli', log_level=LogLevel.DEBUG)
    runner = CliRunner()
    message_regex = "Usage: ranchercli service delete [OPTIONS]"
    with EnvironContext(LOG_LEVEL='TRACE',
                        RANCHER_URL='https://rancher.dev.cu.edu',
                        RANCHER_ENV='ODIN-DEV',
                        RANCHER_STACK='odin-sandbox',
                        RANCHER_SERVICE='odin-api'):
        result = runner.invoke(ranchercli.cli, ['service', 'delete', '-h'])
        logger.info('OUTPUT:\r\n\r\n%s' % result.output)
        assert result.exit_code == 0
        assert message_regex in result.output

