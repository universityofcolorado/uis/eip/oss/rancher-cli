from setuptools import setup


setup(name='rancher-cli',
      version='4.0.0',
      description='Command line interface (CLI) to interact with Rancher from your GitLab CI/CD pipeline, or anywhere.',
      author='kemsar',
      license='MIT',
      packages=['ranchercli'],
      zip_safe=False,
      install_requires=[
          'wheel',
          'click',
          'requests',
          'colorama',
          'sakstig'
      ],
      tests_require=[
          'pytest',
          'cli_test_helpers'
      ],
      entry_points={
          'console_scripts': [
              'rancher-cli = ranchercli.cli:cli'
          ]
      }
      )
