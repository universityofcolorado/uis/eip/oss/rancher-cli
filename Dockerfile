FROM python:3.9-slim
RUN useradd --create-home --shell /bin/bash rancher_user
WORKDIR /home/rancher_user
COPY . /rancher-cli
RUN ls -la / && cd /rancher-cli && python setup.py install
USER rancher_user
CMD ["bash"]
